###--- Instructions ---###
# Call this project with a single argument, the sub folder where you want to store the data. The default argument should simply be "main" (that subfolder should already be created).

### Tells how many threads are in use. ###
println("Threads in use: ",Threads.nthreads())

### Fetches the required packages ###
using DifferentialEquations
using DiffEqBiological
using DiffEqParameters
using SimulationSupport
using Statistics
using DelimitedFiles

### Get the location of the folders to write to ###
include("folder_locations.jl");

### Sets where to save files to, and where to load the model from. ###
include("model_declaration.jl");

### File manipulation ###
function save_sols(sols,nametag,lysozyme,sigV)
    writedlm(data_folder*"stochastic_bifurcation_diagram/"*string(nametag)*"/solutions_"*string(lysozyme)*"_"*string(sigV)*".csv", sols, ',')
end;

### Required functions ###
function template_points(m,l,L)
    sols_template = monte(σV_model,(0.,Float64(l)),m,p_steps=(:L,(50.,L)),adaptive=false,dt=0.002,saveat=1.)
    data_points = [];
    for sol in sols_template, t = 51:Int64(l)
        push!(data_points, sol.u[t])
    end
    return sort(data_points);
end
function split_templates(templates,grid_pre)
    grid = [Array(grid_pre)...,1000]
    output_grid = map(i -> Vector{Vector{Float64}}(),1:length(grid_pre))
    curr_val = grid[1]; next_val = grid[2]; curr_idx = 1;
    for i = 1:length(templates)
        val = templates[i][1]
        while abs(next_val-val) < abs(curr_val-val)
            curr_idx += 1
            curr_val = next_val
            next_val = grid[curr_idx+1]
        end
        push!(output_grid[curr_idx],templates[i])
    end
    return output_grid
end
function pick_template(template_vector,grid_idx,grid_value)
    (length(template_vector[grid_idx]) > 0) && (return sort(map(it -> rand(template_vector[grid_idx]), 1:3))[2])
    new_idx = argmin(abs.(map(i -> isempty(template_vector[i]) ? 10000 : i , 1:length(template_vector)) .-grid_idx))
    base_value = rand(template_vector[new_idx])
    scale_factor = grid_value / base_value[1]
    return base_value * scale_factor
end;

### Declares base parameters ###
nametag = ARGS[1]
grid_L = 0.0125:v:1.9875
grid_L = 1.2375:0.025:1.9875
grid_σV = 0.05:0.1:24.95
data = Array{Float64}(undef, length(grid_L), length(grid_σV))
n = 8; its = ceil(Int64,n/4);
t = 1000;

### Generate the data ###
@time for L in grid_L
    println("Lysosyme strength: ", L);
    refresh!(); templates = template_points(2,2500,L);
    template_vector = split_templates(templates,grid_σV)
    σV_model[:L] = L
    @time for i = 1:length(grid_σV)
        (((50-25)%50)==0) && (println(grid_σV[i]))
        sol_array = []
        for iteration = 1:its
            u0 = pick_template(template_vector,i,grid_σV[i])
            prob = SDEProblem(σV_network,u0,(0.,t),deepcopy(σV_model))
            ensemble_prob = EnsembleProblem(prob,prob_func=(p,i,r)->p)
            dt=0.001
            (L <= 0.45)&&(grid_σV[i]>=18.)&&(dt=0.00065)
            (L <= 0.1)&&(grid_σV[i]>=13.)&&(dt=0.00033)
            (L <= 0.02)&&(grid_σV[i]>=8.)&&(dt=0.00033)
            sols = solve(ensemble_prob,ImplicitEM(),EnsembleThreads();trajectories=4,callback=CallbackSet(positive_domain()),adaptive=false,dt=dt,saveat=1.)
            foreach(sol -> (sol.retcode == :Success)&&(push!(sol_array, first.(sol.u))), sols)
        end
        save_sols(sol_array,nametag,L,grid_σV[i]);
    end
end
