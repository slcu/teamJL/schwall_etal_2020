###--- Instructions ---###
# Call this project with a single argument, the sub folder where you want to store the data. The default argument should simply be "main" (that subfolder should already be created).

###--- PREPARATIONS ---###

### Fetches the Required Packages ###
using DifferentialEquations
using DiffEqBiological
using DiffEqParameters
using SimulationSupport
using DelimitedFiles

### Get the location of the folders to write to ###
include("folder_locations.jl");

### Declares the Model ###
include("../model_declaration.jl");

### Make simulation over several different memory lengths ###
activation_thres = 11.
simulation_length = 3000.
function simulate_holidays_reset(holidays,m,folder,reset_vars)
    template_inactive = load_simulations(data_folder*determine_memory_carrier/*"sols_template_inactive")
    template_active = load_simulations(data_folder*determine_memory_carrier/*"sols_template_active")

    reset_values = []
    for holiday in holidays
        print(holiday,"   ")
        prob_func=(p,i,r)-> (new_u0 = template_active.u[rand(1:template_active.m),rand(500:template_active.l),:]; @. p.u0 = new_u0; p;)
        prob = EnsembleProblem(SDEProblem(σV_model.model,zeros(3),(0.,simulation_length+holiday),deepcopy(σV_model)),prob_func=prob_func)
        cbs = CallbackSet(positive_domain(),reset_reactant_cb(deepcopy(template_inactive.u[:,50:end,:]),holiday,reset_vars),par_step(:L,holiday,1),terminate_sim(activation_thres,holiday))
        sols = solve(prob,ImplicitEM(),EnsembleThreads();trajectories=m,callback=cbs,tstops=[holiday],saveat=1.,maxiters=50000000)
        writedlm(folder*"sols_"*string(holiday)*".csv", map(sol -> first.(sol.u[:]), sols[:]), ',')
        push!(reset_values,map(sol -> sol.t[end]-holiday,sols[:]))
    end
    writedlm(folder*"reset_times.csv", reset_values, ',')
end;

### Callbacks ###
# At a designated point in time, take a single reactant, and resets it value to a random value from a template.
function reset_reactant_cb(reset_values::Array{Float64,3},time,reactants)
    condition(u,t,integrator) = (t==time)
    affect!(integrator) = foreach(reactant -> integrator.u[reactant] = rand(reset_values[:,:,reactant]), reactants)
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end
function terminate_sim(thres_conc,thres_time);
    condition(u,t,integrator) = (u[1]>thres_conc)&&(t>thres_time)
    affect!(integrator) = terminate!(integrator)
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end;

### Sets the required parameters ###
nametag = ARGS[1]
folder = data_folder*determine_memory_carrier/*nametag
holidays = 75:25:700
m = 100;

### Make templates ###
# (only needs to be run once)
if false
    σV_model[:L] = 1.
    sols_template_active = monte(σV_model,(0.,700),200,starting_steady_state_number=3,adaptive=false,dt=0.002,saveat=1.)
    save_simulations(sols_template_active,data_folder*determine_memory_carrier/*"sols_template_active"); sols_reset_SigV = nothing
    σV_model[:L] = 0.
    sols_template_inactive = monte(σV_model,(0.,250),200,adaptive=false,dt=0.002,saveat=1.)
    save_simulations(sols_template_inactive,data_folder*determine_memory_carrier/*"sols_template_inactive"); sols_reset_SigV = nothing;
end

### Runs simulations ###
@time simulate_holidays_reset(holidays,m,folder*"/SigV/",[1]);
@time simulate_holidays_reset(holidays,m,folder*"/SigVRsiV/",[3]);
@time simulate_holidays_reset(holidays,m,folder*"/Null/",[]);
@time simulate_holidays_reset(holidays,m,folder*"/RsiV/",[2]);
