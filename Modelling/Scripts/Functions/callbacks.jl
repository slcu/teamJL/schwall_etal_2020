#A callback for making a step increase in a single parameter.
function par_step(par,step_time,step_value)
    condition(u,t,integrator) = (t==step_time)
    affect!(integrator) = integrator.p[par] += step_value
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end
#A callback for making several step increases, potentially over several parameters.
function par_steps(p_steps)
    output = Vector{DiscreteCallback}()
    for steps in internal_my_split(p_steps), step in steps[2:end]
        push!(output,par_step(steps[1],step...))
    end
    return output
end
#A callback for making a step increase in a single variable.
function var_step(var,step_time,step_value)
    condition(u,t,integrator) = (t==step_time)
    affect!(integrator) = integrator.u[findfirst(integrator.p.model.syms.==var)] += step_value
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end
#A callback for making several step increases, potentially over several variables.
function var_steps(v_steps)
    output = Vector{DiscreteCallback}()
    for steps in internal_my_split(v_steps), step in steps[2:end]
        push!(output,var_step(steps[1],step...))
    end
    return output
end
#A callback for keeping a simulation within the positive domain.
function positive_domain()
    condition(u,t,integrator) = minimum(u) < 0
    affect!(integrator) = integrator.u .= integrator.uprev
    return DiscreteCallback(condition,affect!,save_positions = (false,false))
end;

#Splits an array acording to my desires.
function internal_my_split(array)
    starts = findall(typeof.(array).==Symbol)
    ends = [starts[2:end]...,length(array)+1].-1
    return [array[starts[i]:ends[i]] for i in 1:length(starts)]
end
